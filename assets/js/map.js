require('leaflet/dist/leaflet');

// Leaflet
$( window ).on("load", function() {
    var mymap = L.map('mapId').setView([45.173861, 5.730778], 12);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        id: 'mapbox.streets'
    }).addTo(mymap);
    L.marker([45.173861, 5.730778]).addTo(mymap)
});
