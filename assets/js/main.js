const $ = require('jquery/dist/jquery');

global.$ = global.jQuery = $;

require('bootstrap/dist/js/bootstrap');
require('@fortawesome/fontawesome-free/js/all.min.js');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('../scss/imports.scss');

window.onload = function() {
    init();
};
function init() {
    setTimeout(function() { $('.h1').removeClass("loading"); }, 500);
}

//EE
var down = {};
$(document).keydown(function(e) {
    down[e.keyCode] = true;
}).keyup(function(e) {
    if (down[68] && down[77] && down[82]) {
        $('*').addClass('rotate')
            setTimeout(() => {
                $('*').removeClass('rotate')
            }, 3000);
    }
    down[e.keyCode] = false;
});
