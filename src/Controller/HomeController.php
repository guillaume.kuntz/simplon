<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Event;
use App\Entity\Promo;

/**
 * @Route("/")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $events = $this->getDoctrine()->getRepository(Event::class);
        $promos = $this->getDoctrine()->getRepository(Promo::class);


        return $this->render('home/index.html.twig', [
            'events' => $events->findAll(),
            'promosLast' => $promos->getThreeLastPromo()
        ]);
    }
}
