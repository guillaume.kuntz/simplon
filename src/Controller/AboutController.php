<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Promo;

class AboutController extends AbstractController
{
    /**
     * @Route("/a-propos", name="about")
     */
    public function index()
    {
        $promos = $this->getDoctrine()->getRepository(Promo::class);

        return $this->render('about/index.html.twig', [
            'promosLast' => $promos->getThreeLastPromo()
        ]);
    }
}
