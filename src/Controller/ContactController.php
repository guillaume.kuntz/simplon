<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Promo;
use App\Form\ContactFormType;
use App\Entity\Contact;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $promos = $this->getDoctrine()->getRepository(Promo::class);
        // $partners = $this->getDoctrine()->getRepository(Partner::class);
        $contact = new contact;
        $form = $this->createForm(ContactFormType::class)
        ->add('Envoyer', SubmitType::class);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()){
            $contact = $form->getData();
            var_dump($contact);





            $message = (new \Swift_Message());
            $message->setSubject($contact->getSubject())
            ->setFrom($contact->getEmailFrom())
            ->setBody($contact->getContent())
            ->setTo('j.giraud@live.fr');




            $this->addFlash(
                'notice',
                'Email sent Successfullyliuoiulkiuo'

            );
            $mailer->send($message);

            return $this->redirectToRoute('contact');
         };

        return $this->render('/contact/index.html.twig', [
            'form' => $form->createView(),
            'promosLast' => $promos->getThreeLastPromo()
        ]);



    }
}
