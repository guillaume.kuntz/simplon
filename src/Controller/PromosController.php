<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Promo;

class PromosController extends AbstractController
{
    /**
     * @Route("/promos", name="promos")
     */
    public function index()
    {
        $promos = $this->getDoctrine()->getRepository(Promo::class);

        return $this->render('promos/promos-all.html.twig',
        [
            'promos' => $promos->findAll(),
            'promosLast' => $promos->getThreeLastPromo(),
        ]);
    }
    /**
     * @Route("/promos/{id}", name="promo-details")
     */
    public function promo($id)
    {
        $promo = $this->getDoctrine()->getRepository(Promo::class);
        $students = $promo->findOneById($id)->getStudents();
        $promoTitle = $promo->findOneById($id)->getName();

        return $this->render('promos/promo-details.html.twig', [
            'promoTitle' => $promoTitle,
            'promosLast' => $promo->getThreeLastPromo(),
            'students' => $students,
        ]);
    }
}
