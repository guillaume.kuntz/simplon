<?php

namespace App\Repository;

use App\Entity\EventIcon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EventIcon|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventIcon|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventIcon[]    findAll()
 * @method EventIcon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventIconRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EventIcon::class);
    }

    // /**
    //  * @return EventIcon[] Returns an array of EventIcon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventIcon
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
