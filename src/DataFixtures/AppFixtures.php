<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Promo;
use App\Entity\Student;
use App\Entity\EventIcon;
use App\Entity\Event;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $desc = "<p>Dum apud Persas, ut supra narravimus, perfidia regis motus agitat insperatos, et in eois tractibus bella rediviva consurgunt, anno sexto decimo et eo diutius post Nepotiani exitium, saeviens per urbem aeternam urebat cuncta Bellona, ex primordiis minimis ad clades excita luctuosas, quas obliterasset utinam iuge silentium! ne forte paria quandoque temptentur, plus exemplis generalibus nocitura quam delictis.</p>";

        $promoArr = [];

        $promoTitle = ["Développeur.se Web", "Réferent(e) Digital", "Développeur.se Data", "Développeur.se Data IA", "Coder et déployer une application web simple", "Animateur.rice de tiers-lieu de fabrication numérique"];
        $promoImg = ["dev_web.png", "ref_digital.png", "dev_data.png", "dev_data_ia.jpg", "dev_simple.jpg", "anim_fabrique.png"];
        // PROMO
        for ($i=0; $i <sizeof($promoTitle); $i++) {
            $randY = random_int(2015, 2017);
            $randM = random_int(1 , 12);
            $randD = random_int(1 , 30);

            $date = new \DateTime();
            $endDate = new \DateTime();
            if ($i == 0) {
                $date->setDate("2018", "12", "10");
                $endDate->setDate("2019", "6", "10");
            }else {
                $date->setDate(strval($randY), strval($randM), strval($randD));
                $endDate->setDate(strval($randY+1), strval($randM), strval($randD));
            }

            $promo = new Promo();
            if ($i == 0) {
                $promo->setName($promoTitle[$i] . " - " . "2018");
            }else {
                $promo->setName($promoTitle[$i] . " - " . strval($randY));
            }
            $promo->setDescription($desc);
            $promo->setStartDate($date);
            $promo->setEndDate($endDate);
            $promo->setImage($promoImg[$i]);
            $promo->setUpdatedAt(new \DateTime("now"));

            $promoArr[] = $promo;

            $manager->persist($promo);
        }


        // $manager->flush();

        // STUDENT
        $dataStudent = [
            ["PunchMan", "One"],
            ["Bruce", "Lee"],
            ["Madame", "Doutfire"],
            ["Jacqueline", "Henderson"],
            ["Ivan", "Martins"],
            ["Elsa", "Perkins"],
            ["Robert", "Richardson"]
        ];
        $dataDev = [
            ["Nassim", "Benhaddou", "Nassim_Benhaddou.jpg"],
            ["Dylan", "Bouchet", "Dylan_Bouchet.jpg"],
            ["Célèste", "Cinti", "Célèste_Cinti.jpg"],
            ["Lionel", "Clerget", "Lionel_Clerget.jpg"],
            ["Bakary", "Diallo", "Bakary_Diallo.jpg"],
            ["Rémi", "Drago", "Rémi_Drago.jpg"],
            ["Anne-Laure", "Dubois", "Anne-Laure_Dubois.jpg"],
            ["Ilhem", "Ettabane Kadri", "Ilhem_Ettabane_Kadri.jpg"],
            ["Josselin", "Giraud", "Josselin_Giraud.jpg"],
            ["Grezgorz", "Grobelny", "Grezgorz_Grobelny.jpg"],
            ["André", "Isseta", "André_Isseta.jpg"],
            ["Guillaume", "Kuntz", "Guillaume_Kuntz.jpg"],
            ["Diane", "Lamant", "Diane_Lamant.jpg"],
            ["Nadia", "Mnasri", "Nadia_Mnasri.jpg"],
            ["Mubarak", "Mohamed Abdalshafi", "Mubarak_Mohamed_Abdalshafi.jpg"],
            ["Alexandre", "Riera", "Alexandre_Riera.jpg"],
            ["Robin", "Rodriguez", "Robin_Rodriguez.jpg"],
            ["Ibrahim", "Sanhaji", "Ibrahim_Sanhaji.jpg"],
        ];
        for ($i=0; $i < sizeof($promoArr); $i++) {
            if ($i == 0) {
                for ($j=0; $j < sizeof($dataDev); $j++) {
                    $student = new Student();
                    $student->setFirstName($dataDev[$j][0]);
                    $student->setLastName($dataDev[$j][1]);
                    $student->setImage($dataDev[$j][2]);
                    $student->setUpdatedAt(new \DateTime("now"));
                    $student->setPromo($promoArr[$i]);
                    $manager->persist($student);
                }
            }else {
                for ($j=0; $j < sizeof($dataStudent); $j++) {
                    $num = $j+1;
                    $student = new Student();
                    $student->setFirstName($dataStudent[$j][0]);
                    $student->setLastName($dataStudent[$j][1]);
                    $student->setImage("student_$num.jpg");
                    $student->setUpdatedAt(new \DateTime("now"));
                    $student->setPromo($promoArr[$i]);
                    $manager->persist($student);
                }
            }
        }

        // $manager->flush();

        $iconBullHorn;

        // EVENTICON
        $dataIcon = [
            ["calendar", "fa fa-calendar"],
            ["bullhorn", "fa fa-bullhorn"],
            ["exclamation-triangle", "fa fa-exclamation-triangle"],
            ["heart", "fa fa-heart"],
        ];
        for ($i=0; $i < sizeof($dataIcon); $i++) {
            $icon = new EventIcon();
            $icon->setName($dataIcon[$i][0]);
            $icon->setCssClass($dataIcon[$i][1]);
            if ($i == 1) {
                $iconBullHorn = $icon;
            }
            $manager->persist($icon);
        }

        // $manager->flush();

        // EVENT
        for ($i=0; $i < 5 ; $i++) {
            $num = $i+1;
            $randY = random_int(2018, 2021);
            $randM = random_int(1 , 12);
            $randD = random_int(1 , 30);
            $startDate = new \DateTime(strval($randY)."-".strval($randM)."-".strval($randD)." 18:00");
            $event = new Event();
            $event->setTitle("Evenement N° $num");
            $event->setDescription($desc);
            $event->setStartDate($startDate);
            $event->setImage("event_$num.jpg");
            $event->setUpdatedAt(new \DateTime('now'));
            $event->setIcon($iconBullHorn);
            $manager->persist($event);
        }

        $manager->flush();
    }
}
